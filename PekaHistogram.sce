// Author :Jcob21   
// environment : SCILAB 
// Function servs for meaking histogram


function PeakHistogram(Values, DigitPoints, FileName) 
    scf(0)
    
    x=[0:1:255]
    plot2d3(x, Values ,style=2,rect=[110, 0, 125, DigitPoints * 1.1])  // size of Input Matrix , Input Matrix, color(psitive intiger), rect = (min x, min y, max x max y)
    xtitle("values of collected data ")
    xs2pdf(0,FileName)
endfunction