//Author : MinoPB
//date of creating : 2020-01-04
//executive script for exercise no. 9
//the main reason for the existence of this script are problems with the analysis function with a large number of elements


File_Name = 'BigData.dat'           // File's Name
dt = 5e-10                          // time quantum in sampling
Start_Pos = 0                       // coursosr's starat position 
XX =0                               


while Start_Pos < 30e8 do           
                                                 
    [PeakCount, PulseLvl, NoiseLvl, Track, Summary,pos] = AC_Signal_Analize(File_Name, dt, Start_Pos);
    XX = XX + PeakCount
    Start_Pos = Start_Pos + 1e6
    
end
